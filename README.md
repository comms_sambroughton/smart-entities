##INSTALLATION

####Creating an entity:
- Run `php artisan make:smart {name}` from the command line.
- The command accepts an optional second argument for the entity's `{domain}`. The domain will determine the repository file structure 
(repos within the same domain are grouped) and default Service. It defaults to the `{name}` argument.
- The command accepts the following options:
    -   `-c` or `--controller`
    -   `-r` or `--resource` for a resourceful controller
    -   `-s` or `--service`
    -   `-m` or `--migration`
    
####Deleting an entity:
- Run `php artisan del:smart {name}` from the command line.
- The command accepts an optional second argument for the entity's `{domain}`.
- The command accepts a `-f` or `--force` option if you wish to delete the domain's repo folders (along with anything in them)

####Creating a user entity:
- Run `php artisan make:smartauth` from the command line.
- Add the following to App\Providers\AuthServiceProvider in the boot function below $this->registerPolicies():
     `Auth::provider('smartprovider', function() {
         return new SmartAuthProvider($this->app['hash'],
          app()->make(\App\Services\UserService::class));
     });`
 
 - In config/auth:
     1) Change guards.web.provider to 'smart'
     2) Add the following to the providers array:
     `'smart' => [
         'driver' => 'smartprovider',
         'model' => App\Entities\UserEntity::class,
     ]`

##


##TODO

 - check withTrashed as a global scope?
 - put fillable attributes on the entity
 - replace references to App with DummyRootNamespace