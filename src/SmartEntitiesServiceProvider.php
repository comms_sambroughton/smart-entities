<?php

namespace CommsExpress\SmartEntities;

use CommsExpress\SmartEntities\Commands\DeleteSmartEntityCommand;
use CommsExpress\SmartEntities\Commands\MakeSmartAuthCommand;
use CommsExpress\SmartEntities\Commands\MakeSmartEntityCommand;
use Illuminate\Support\ServiceProvider;

class SmartEntitiesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->app->bind('command.make:smart', MakeSmartEntityCommand::class);
            $this->app->bind('command.make:smartauth', MakeSmartAuthCommand::class);
            $this->app->bind('command.del:smart', DeleteSmartEntityCommand::class);
            $this->commands([
                'command.make:smart',
                'command.make:smartauth',
                'command.del:smart',
            ]);
        }
    }

    public function register()
    {

    }

    public static $containerMap = [];

    public static function containerMap(array $map = null, $merge = true)
    {
        if (is_array($map)) {
            static::$containerMap =
                $merge && static::$containerMap
                    ? $map + static::$containerMap
                    : $map;
        }

        return static::$containerMap;
    }

    public static function getContainerFor($object)
    {
        $class = get_class($object);
        if (! array_has(static::$containerMap, $class)) throw new \Exception("Unregistered class [{$class}] - please register using SmartEntityServiceProvider::containerMap() in a service provider.");

        return static::$containerMap[$class];
    }
}