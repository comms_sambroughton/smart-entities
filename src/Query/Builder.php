<?php

namespace CommsExpress\SmartEntities\Query;

use CommsExpress\SmartEntities\Entities\BaseEntity;
use CommsExpress\SmartEntities\Repositories\BaseRepositoryContract;
use CommsExpress\SmartEntities\Traits\BuildsEntities;
use Illuminate\Support\Collection;

class Builder
{
    use BuildsEntities;

    protected $wheres = [];
    protected $withTrashed = false;
    protected $forRelation;
    private $repo;
    private $entity;
    protected $passthru = [
        'take',
        'where',
        'with',
        'has',
    ];

    public function __construct(BaseEntity $entity, BaseRepositoryContract $repo)
    {
        $this->repo = $repo;
        $this->entity = $entity;
    }

    public function first()
    {
        $this->take(1);
        $result = $this->get();
        return $result->first();
    }

    public function all()
    {
        return collect($this->get());
    }

    public function find($key)
    {
        $this->where($this->entity->getKeyName(), $key);
        $result = $this->get();
        return $result->first();
    }

    public function orWhere($column, $operator = null, $value = null)
    {
        $this->where($column, $operator, $value, 'or');
        return $this;
    }

    public function withTrashed()
    {
        $this->withTrashed = true;
        return $this;
    }

    public function get()
    {
        if ($this->entity->hasSoftDeletes() && !$this->withTrashed)
            $this->where('deleted_at', '=', null);

        $result = $this->repo->get();

        if (!$result) return null;
        $result =  $result instanceof Collection
            ? $this->buildEntities($result)
            : $this->buildEntity($result);

        if ($this->forRelation) {

            $result = $result->first()->{$this->forRelation};
        }
        return $result;
    }

    public function forRelation(string $name)
    {
        $this->forRelation = $name;
        return $this;
    }

    public function __call($method, $parameters)
    {
        if (in_array($method, $this->passthru)) {
            $this->repo->$method(...$parameters);
            return $this;
        }
        throw new \Exception('Method [' . $method . '] does not exist on ' . static::class);
    }
}