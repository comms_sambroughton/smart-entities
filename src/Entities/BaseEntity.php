<?php

namespace CommsExpress\SmartEntities\Entities;

use CommsExpress\SmartEntities\Entities\Concerns;
use CommsExpress\SmartEntities\Query\Builder as QueryBuilder;
use CommsExpress\SmartEntities\Relations\Pivot;
use CommsExpress\SmartEntities\Repositories\BaseRepositoryContract;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Support\Collection;
use JsonSerializable;

abstract class BaseEntity implements Jsonable, JsonSerializable
{
    use Concerns\InteractsWithRepository;

    public $relations = [];
    public $unsavedAddedRelations = [];
    public $unsavedRemovedRelations = [];
    protected $pivot;
    protected $repo;
    protected $attributes = [];
    protected $primaryKey = 'id';

    public function __construct(BaseRepositoryContract $repo)
    {
        $this->repo = $repo;
    }

    public static function create(array $attributes = [])
    {
        $entity = static::make($attributes);
        $entity->save();
        return $entity;
    }

    public static function make(array $attributes = [])
    {
        $entity = app()->make(static::class);
        $entity->setAttributes($attributes);
        return $entity;
    }

    public function update(array $attributes = [])
    {
        $this->setAttributes($attributes);
        $this->save();
        return $this;
    }

    public function setAttributes(array $attributes = [])
    {
        foreach ($attributes as $key => $value) {
            if (!$value) continue;
            $this->$key = $value;
        }
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getAttribute($key)
    {
        return $this->attributes[$key];
    }

    public function getRelation($key)
    {
        if (!isset($this->relations[$key])) {
            // Move this to InteractsWithRepository
            $relation = $this->repo->getRelation($key);
            if (!$relation) return null;
            $this->relations[$key] = $relation instanceof Collection
                ? $this->buildEntities($relation)
                : $this->buildEntity($relation);
        }
        return $this->relations[$key];
    }

    public function getKeyName()
    {
        return $this->primaryKey;
    }

    public function getKey()
    {
        return $this->{$this->getKeyName()};
    }

    public function newQuery()
    {
        // use fresh repo
        return new QueryBuilder($this, $this->repo);
    }

    public function getBlank()
    {
        return app()->make(static::class);
    }

    public function setPivot(Pivot $pivot)
    {
        $this->pivot = $pivot;
    }

    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    public function __get($key)
    {
        if ($key == 'pivot') return $this->pivot;

        return $this->repo->hasRelation($key)
            ? $this->getRelation($key)
            : $this->getAttribute($key);
    }

    public static function __callStatic($method, $parameters)
    {
        return app()->make(static::class)->$method(...$parameters);
    }

    public function getRepo()
    {
        return $this->repo;
    }

    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }
        return $json;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray()
    {
        return array_merge($this->attributes, $this->relations);
    }
}