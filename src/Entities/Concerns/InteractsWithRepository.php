<?php

namespace CommsExpress\SmartEntities\Entities\Concerns;

use Carbon\Carbon;
use CommsExpress\SmartEntities\Entities\BaseEntity;
use CommsExpress\SmartEntities\Relations\Builder as RelationBuilder;

trait InteractsWithRepository
{
    public static $softDeletes = false;
    public $exists = false;
    public function save()
    {
        $this->updateRepoAttributes();
        $this->updateRepoRelations();

        // wrap in try-catch
        $this->repo->save();

        $this->unsavedAddedRelations = [];
        if (!$this->exists) $this->initialise();
    }

    public function delete()
    {
        static::$softDeletes
            ? $this->softDelete()
            : $this->repo->delete();
        $this->exists = false;
    }

    protected function softDelete()
    {
        $this->update(['deleted_at' => Carbon::now()]);
    }

    public function restore()
    {
        $this->update(['deleted_at' => null]);
        $this->exists = true;
    }

    protected function updateRepoAttributes()
    {
        $this->repo->setAttributes($this->attributes);
    }

    protected function updateRepoRelations()
    {
        foreach ($this->unsavedAddedRelations as $key => $value) {
            $this->updateRelation($key, $value);
        }

        foreach ($this->unsavedRemovedRelations as $key => $value) {
            $this->updateRelation($key, $value, true);
        }
    }

    protected function updateRelation($name, $value, $remove = false)
    {
        if(!is_array($value))
        {
            return !$remove ? $this->repo->associateRelation($name, $value->repo) : $this->repo->dissociateRelation($name);
        }

        return !$remove ? $this->attachArrayOfRelations($name, $value) : $this->detachArrayOfRelations($name, $value);
    }

    private function attachArrayOfRelations($name, $relations)
    {
        $this->repo->attachRelations($name, collect($relations)->mapWithKeys(function ($entity) {
            if ($entity instanceof BaseEntity) {
                $pivot = $entity->pivot
                    ? $entity->pivot->toArray()
                    : [];
                return [$entity->getKey() => $pivot];
            } else {
                return [$entity['key'] => $entity['pivotData']];
            }
        }));
    }

    private function detachArrayOfRelations($name, $relations)
    {
        $this->repo->detachRelations($name, collect($relations)->map(function ($entity) {
            return $entity->getKey();
        }));
    }

    protected function initialise()
    {
        $this->setAttribute($this->getKeyName(), $this->repo->getKey());
        $this->exists = true;
        $this->setTimestamps();
    }

    public function hasSoftDeletes()
    {
        return static::$softDeletes;
    }

    public function hasTimestamps()
    {
        $attributes = $this->repo->getAttributes();

        return (isset($attributes['created_at']) || isset($attributes['updated_at']));
    }

    protected function setTimestamps()
    {
        if(!$this->hasTimestamps()) return;

        $attributes = $this->repo->getAttributes();

        $this->setAttribute('created_at', $attributes['created_at']);
        $this->setAttribute('updated_at', $attributes['updated_at']);
    }

    public function __call($method, $parameters)
    {
        if ($this->repo->hasRelation($method)) {
            return new RelationBuilder($method, $this, $this->repo);
        }

        return $this->newQuery()->$method(...$parameters);
    }
}