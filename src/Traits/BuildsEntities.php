<?php

namespace CommsExpress\SmartEntities\Traits;

use CommsExpress\SmartEntities\Relations\Pivot;
use CommsExpress\SmartEntities\Repositories\BaseRepositoryContract;
use CommsExpress\SmartEntities\SmartEntitiesServiceProvider;
use Illuminate\Support\Collection;

trait BuildsEntities
{
    public function buildEntities(Collection $repos)
    {
        return $repos->map(function ($repo) {
            return $this->buildEntity($repo);
        })->filter();
    }

    public function buildEntity(BaseRepositoryContract $repo)
    {
        $entityClass = SmartEntitiesServiceProvider::getContainerFor($repo);
        $entity = new $entityClass($repo);

        $entity->setAttributes($repo->getAttributes());

        if($pivotData = $repo->getPivotData())
            $entity->setPivot(new Pivot($pivotData));

        $entity->exists = true;
        return $entity;
    }
}