<?php

namespace CommsExpress\SmartEntities\Repositories\Eloquent;

use CommsExpress\SmartEntities\Repositories\BaseRepositoryContract;
use CommsExpress\SmartEntities\SmartEntitiesServiceProvider;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

abstract class BaseRepository
{
    private $model;
    protected $query;

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
        $this->query = $model->newInstance();
    }

    public function setAttributes(array $attributes = []): void
    {
        foreach ($attributes as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getAttributes(): array
    {
        return $this->model->getAttributes();
    }

    public function save(): void
    {
        $this->model->save();
    }

    public function delete(): void
    {
        $this->model->delete();
        $this->model = $this->model->newInstance();
    }

    public function getKey()
    {
        return $this->model->getKey();
    }

    public function take(int $number): void
    {
        $this->query = $this->query->take($number);
    }

    public function where($column, $operator = null, $value = null, $boolean = 'and'): void
    {
        if (Schema::hasColumn($this->model->getTable(), $column)) {
            $this->query = $this->query->where($column, $operator, $value, $boolean);
            return;
        }

        $exploded_column = explode('.', strrev($column), 2);

        $sub_column = strrev($exploded_column[0]);
        if (count($exploded_column) == 2)
            $relation = strrev($exploded_column[1]);

        if (isset($relation)) {
            $this->whereHas($relation, $sub_column, $operator, $value);
            return;
        }

        throw new \Exception($this->model->getTable() . ' does not have a [' . $column . '] column.');
    }

    protected function whereHas($relation, $column, $operator, $value)
    {
        $this->query = $this->query->whereHas($relation, function ($query) use ($column, $operator, $value) {
            $query->where($column, $operator, $value);
        });
    }

    public function has($relation): void
    {
        $this->query = $this->query->has($relation);
    }

    public function with($relation): void
    {
        $this->query = $this->query->with($relation);
    }

    public function get()
    {
        $queryResult = $this->query->get();
        $this->query = $this->model;
        return $queryResult instanceof Collection
            ? $this->wrapModels($queryResult)
            : $this->wrapModel($queryResult);
    }

    public function hasRelation(string $name): bool
    {
        return $this->model->hasRelation($name);
    }

    public function getRelationClass(string $name)
    {
        return $this->model->getRelationClass($name);
    }

    public function getRelation(string $key)
    {
        $relation = $this->model->$key;

        if (!$relation) return null;
        return $relation instanceof Collection
            ? $this->wrapModels($relation)
            : $this->wrapModel($relation);
    }

    public function createRelation(string $key, array $attributes = []): BaseRepositoryContract
    {
        $model = $this->model->$key()->create($attributes);
        if ($this->model->$key() instanceof BelongsTo) {
            $this->model->$key()->associate($model)->save();
        }
        $repo = SmartEntitiesServiceProvider::getContainerFor($model);
        return new $repo($model);
    }

    public function associateRelation(string $name, BaseRepositoryContract $relation): void
    {
        $this->model->{$name}()->associate($relation->model);
    }

    public function dissociateRelation(string $name): void
    {
        $this->model->{$name}()->dissociate();
    }

    public function attachRelations(string $name, $relations): void
    {
        $this->model->{$name}()->attach($relations);
    }

    public function detachRelations(string $name, $relations): void
    {
        $this->model->{$name}()->detach($relations);
    }

    public function getPivotData(): array
    {
        $relations = $this->model->getRelations();
        return isset($relations['pivot'])
            ? $relations['pivot']->toArray()
            : [];
    }

    public function updateExistingPivot($name, $id, $attributes): void
    {
        $this->model->{$name}()->updateExistingPivot($id, $attributes);
    }

    public function sync($ids,$name)
    {
        $this->model->{$name}()->sync($ids);
    }

    protected function wrapModel(BaseModel $model)
    {
        $repo = SmartEntitiesServiceProvider::getContainerFor($model);
        return new $repo($model);
    }

    protected function wrapModels(Collection $models)
    {
        return $models->map(function ($model) {
            return $this->wrapModel($model);
        });
    }

    public function __set($key, $value): void
    {
        $this->model->$key = $value;
    }

}