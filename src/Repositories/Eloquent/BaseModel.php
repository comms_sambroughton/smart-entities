<?php

namespace CommsExpress\SmartEntities\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    protected $guarded = [];

    public function hasRelation(string $name)
    {
        return !method_exists(self::class, $name)
        && method_exists(static::class, $name);
    }
}