<?php

namespace CommsExpress\SmartEntities\Repositories;

interface BaseRepositoryContract
{
    public function setAttributes(array $attributes = []): void;

    public function getAttributes() : array;

    // Give some feedback (bool?)
    public function save() : void;

    // Give some feedback (bool?)
    public function delete() : void;

    // Restrict the return type somehow
    public function getKey();

    public function take(int $number): void;

    public function where($column, $operator = null, $value = null, $boolean = 'and'): void;

    public function with($relation): void;

    public function has($relation): void;

    // Make sure this is either a Collection or Repository
    // (maybe create a Collection class that shares an interface with BaseRepositories)
    public function get();

    public function hasRelation(string $name): bool;

    // Make sure this is either a Collection or Repository
    // (maybe create a Collection class that shares an interface with BaseRepositories)
    public function getRelation(string $key);

    public function createRelation(string $key, array $attributes = []): BaseRepositoryContract;

    // Give some feedback (bool?)
    public function associateRelation(string $name, BaseRepositoryContract $relation): void;

    // Give some feedback (bool?)
    public function dissociateRelation(string $name): void;

    // Give some feedback (bool?)
    // Standardise relations as array?
    public function attachRelations(string $name, $relations): void;

    // Give some feedback (bool?)
    // Standardise relations as array?
    public function detachRelations(string $name, $relations): void;

    public function getPivotData(): array;

    public function __set($key, $value) : void;
}