<?php

namespace CommsExpress\SmartEntities\Relations;

use CommsExpress\SmartEntities\Entities\BaseEntity;
use CommsExpress\SmartEntities\Repositories\BaseRepositoryContract;
use CommsExpress\SmartEntities\SmartEntitiesServiceProvider;
use CommsExpress\SmartEntities\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;

class Builder
{
    private $entity;
    private $name;
    private $repo;

    public function __construct(string $relation, BaseEntity $entity, BaseRepositoryContract $repo)
    {
        $this->entity = $entity;
        $this->name = $relation;
        $this->repo = $repo;
    }

    public function associate(BaseEntity $relation)
    {
        $this->entity->relations[$this->name] = $relation;
        $this->entity->unsavedAddedRelations[$this->name] = $relation;
        return $this->entity;
    }

    public function dissociate()
    {
        $this->entity->relations[$this->name] = null;
        $this->entity->unsavedRemovedRelations[$this->name] = true;
        return $this->entity;
    }

    public function attach($relation, array $pivotData = [])
    {
        if ($relation instanceof BaseEntity) {
            if (!isset($this->entity->relations[$this->name]))
                $this->entity->relations[$this->name] = collect();
            $relation->setPivot(new Pivot($pivotData));
            $this->entity->relations[$this->name][] = $relation;
        } elseif (is_array($relation)) {
            foreach ($relation as $key => $value) {
                is_array($value)
                    ? $this->attach($key, $value)
                    : $this->attach($value);
            }
            return $this->entity;
        } elseif (is_integer($relation)) {
            $relation = ['key' => $relation, 'pivotData' => $pivotData];
        } else {
            throw new \Exception('Unknown relation type: ' . get_class($relation));
        }

        $this->entity->unsavedAddedRelations[$this->name][] = $relation;
        return $this->entity;
    }

    public function detach(BaseEntity $relation)
    {
        $relationKey = $this->entity->relations[$this->name]->search($relation);
        unset($this->entity->relations[$this->name][$relationKey]);
        $this->entity->unsavedRemovedRelations[$this->name][] = $relation;
        return $this->entity;
    }

    public function create(array $attributes = [])
    {
        $relationRepo = $this->repo->createRelation($this->name, $attributes);
        $relationEntityClass = SmartEntitiesServiceProvider::getContainerFor($relationRepo);
        return app()->make($relationEntityClass)->buildEntity($relationRepo);
    }

    public function update(array $attributes = [])
    {
        $this->entity->{$this->name}->update($attributes);
    }

    public function delete()
    {
        $this->entity->{$this->name}->delete();
        $this->entity->{$this->name}()->dissociate()->save();
    }

    public function updateExistingPivot($relation_id, array $attributes = [])
    {
        $this->updatePivotOnEntity($relation_id, $attributes);
        $this->repo->updateExistingPivot($this->name, $relation_id, $attributes);
    }

    public function sync($ids)
    {
        $this->repo->sync($ids, $this->name);
    }

    protected function updatePivotOnEntity($relation_id, array $attributes):void
    {
        $relation = $this->entity->{$this->name}->filter(function ($relation) use ($relation_id) {
            return $relation->getKey() == $relation_id;
        })->first();

        $relation->pivot->update($attributes);
    }

    public function where($key, $value)
    {
//        return $this->entity->{$this->name}->where()

//        return $this->entity->{$this->name}()->where($key, $value);

        return $this->entity->{$this->name}->filter(function ($r) use ($key, $value) {
           return $r->$key === $value;
        });
    }
}