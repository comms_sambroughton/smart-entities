<?php

namespace CommsExpress\SmartEntities\Relations;

class Pivot
{
    private $attributes;

    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    public function toArray()
    {
        return $this->attributes;
    }

    public function update(array $attributes = [])
    {
        $this->attributes = array_merge($this->attributes, $attributes);
    }

    public function __get($key)
    {
        return $this->attributes[$key];
    }
}