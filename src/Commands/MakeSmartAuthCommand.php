<?php

namespace CommsExpress\SmartEntities\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeSmartAuthCommand extends GeneratorCommand
{
    protected $signature = 'make:smartauth {name=User}
                            {--m|migration : Create a migration for the entity}
                            {--s|service : Create a service for the entity}
                            {--c|controller : Create a controller for the entity}
                            {--r|resource : Create a resourceful controller for the entity}
                            {--orm=eloquent : The ORM the entity will use (defaults to Eloquent).}
                            {--a|all : Generate all files}';

    protected $description = 'Make a Smart Entity';

    protected $views = [
        'auth/login.stub' => 'auth/login.blade.php',
        'auth/register.stub' => 'auth/register.blade.php',
        'auth/passwords/email.stub' => 'auth/passwords/email.blade.php',
        'auth/passwords/reset.stub' => 'auth/passwords/reset.blade.php',
        'layouts/app.stub' => 'layouts/app.blade.php',
        'home.stub' => 'home.blade.php',
    ];


    protected $terms;

    protected function setTerms($name)
    {
        $this->terms = [
            'Entity' => [
                'name' => "{$name}Entity",
                'namespace' => 'Entities',
                'path' => "/Entities/{$name}Entity.php",
                'stub' => '/stubs/user/entity.stub',
            ],
            'Contract' => [
                'name' => "{$name}RepositoryContract",
                'namespace' => "Repositories\\{$name}",
                'path' => "/Repositories/{$name}/{$name}RepositoryContract.php",
                'stub' => '/stubs/user/repocontract.stub',
            ],
            'Eloquent' => [
                'Repository' => [
                    'name' => "{$name}Repository",
                    'namespace' => "Repositories\\{$name}\\Eloquent",
                    'path' => "/Repositories/{$name}/Eloquent/{$name}Repository.php",
                    'stub' => '/stubs/eloquent/repo.stub',
                ],
                'Model' => [
                    'name' => $name,
                    'namespace' => "Repositories\\{$name}\\Eloquent",
                    'path' => "/Repositories/{$name}/Eloquent/{$name}.php",
                    'stub' => '/stubs/user/eloquent/model.stub',
                ],
                'Provider' => [
                    'name' => "{$name}ServiceProvider",
                    'namespace' => "Providers",
                    'path' => "/Providers/{$name}ServiceProvider.php",
                    'stub' => '/stubs/eloquent/provider.stub',
                ],
            ],
            'Service' => [
                'name' => "{$name}Service",
                'namespace' => "Services",
                'path' => "/Services/{$name}Service.php",
                'stub' => '/stubs/user/service.stub',
            ],
            'Controller' => [
                'name' => "{$name}Controller",
                'namespace' => "Http\\Controllers",
                'path' => "/Http/Controllers/{$name}Controller.php",
                'stub' => '/stubs/controller.plain.stub',
            ],
            'ResourcefulController' => [
                'name' => "{$name}Controller",
                'namespace' => "Http/Controllers",
                'path' => "/Http/Controllers/{$name}Controller.php",
                'stub' => '/stubs/controller.stub',
            ],
            'SmartAuthProvider' => [
                'name' => "SmartAuthProvider",
                'namespace' => "Providers",
                'path' => "/Providers/SmartAuthProvider.php",
                'stub' => '/stubs/user/authprovider.stub',
            ]
        ];
    }

    public function handle()
    {
        $name = ucfirst($this->getNameInput());
        $this->setTerms($name);

        $this->input->setOption('service', true);
        $this->input->setOption('resource', true);

        if ($this->option('all')) {
            $this->input->setOption('migration', true);
        }

        $this->type = 'Entity';
        parent::handle();
        $this->type = 'Contract';
        parent::handle();
        $this->type = ucfirst($this->option('orm')) . '.Repository';
        parent::handle();
        if ($this->option('orm') == 'eloquent') {
            $this->type = 'Eloquent.Model';
            parent::handle();
        }
        $this->type = ucfirst($this->option('orm')) . '.Provider';
        parent::handle();

        // Controller option
        if ($this->option('controller')) {
            $this->input->setOption('service', true);
            $this->type = 'Controller';
            parent::handle();
        }
        // Resourceful Controller option
        if ($this->option('resource')) {
            $this->input->setOption('service', true);
            $this->type = 'ResourcefulController';
            parent::handle();
        }

        // Service option
        if ($this->option('service')) {
            $this->type = 'Service';
            parent::handle();
        }

        // Migration option
        if ($this->option('migration') && $this->option('orm') == 'eloquent') {
            $this->createMigration();
        }
        $this->type = 'SmartAuthProvider';
        parent::handle();
        $this->exportViews();
        $this->addRoutes();
        $this->replaceRegisterController();

        // Tell the user to add the service provider to config/app.php
        $this->alert("Make sure to \n
         1) Add {$name}ServiceProvider to the config/app.php 'providers' array.
         2) Register the SmartUserProvider in the AuthServiceProvider (see Readme).
         3) Update config/auth.php with the new provider details (see Readme).");
    }

    protected function qualifyClass($name)
    {
        return array_get($this->terms, $this->type . '.name');
    }

    protected function getStub()
    {
        $stub = array_get($this->terms, $this->type . '.stub');
        return __DIR__ . $stub;
    }

    protected function getPath($name)
    {
        $path = array_get($this->terms, $this->type . '.path');
        return $this->laravel['path'] . $path;
    }

    protected function rootNamespace()
    {
        return parent::rootNamespace() . array_get($this->terms, $this->type . '.namespace');
    }

    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace('DummyNamespace', $this->rootNamespace(), $stub);
        return $this;
    }

    protected function replaceClass($stub, $name)
    {
        if ($this->type == 'Eloquent.Model') {
            $stub = str_replace('dummytablename', snake_case(str_plural($name)), $stub);
        }

        $stub = str_replace('DummyInput', ucfirst($this->getNameInput()), $stub);

        $stub = str_replace('DummyDomain', ucfirst($this->getDomainInput()), $stub);

        return str_replace('DummyClass', $name, $stub);
    }

    protected function createMigration()
    {
        $table = Str::plural(Str::snake(class_basename($this->argument('name'))));

        $this->call('make:migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);
    }

    protected function exportViews()
    {
        $this->createViewDirectories();
        foreach ($this->views as $key => $value) {
            if (file_exists($view = resource_path('views/' . $value))) {
                if (!$this->confirm("The [{$value}] view already exists. Do you want to replace it?")) {
                    continue;
                }
            }

            copy(
                __DIR__ . '/../../../../laravel/framework/src/Illuminate/Auth/Console/Stubs/make/views/' . $key,
                $view
            );
        }
    }

    protected function createViewDirectories()
    {
        if (!is_dir($directory = resource_path('views/layouts'))) {
            mkdir($directory, 0755, true);
        }

        if (!is_dir($directory = resource_path('views/auth/passwords'))) {
            mkdir($directory, 0755, true);
        }
    }

    public function addRoutes(): void
    {
        file_put_contents(base_path('routes/web.php'),
            file_get_contents(__DIR__ . '/../../../../laravel/framework/src/Illuminate/Auth/Console/Stubs/make/routes.stub'),
            FILE_APPEND);
    }

    public function replaceRegisterController(): void
    {
        file_put_contents(
            $this->laravel['path'] . "/Http/Controllers/Auth/RegisterController.php",
            file_get_contents(__DIR__ . '/stubs/registercontroller.stub')
        );
    }

    protected function getDomainInput()
    {
        $domain = $this->getNameInput();
        return ucfirst($domain);
    }
}