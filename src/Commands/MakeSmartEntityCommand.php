<?php

namespace CommsExpress\SmartEntities\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeSmartEntityCommand extends GeneratorCommand
{
    protected $signature = 'make:smart {name} {domain?}
                            {--m|migration : Create a migration for the entity}
                            {--s|service : Create a service for the entity}
                            {--c|controller : Create a controller for the entity}
                            {--r|resource : Create a resourceful controller for the entity}
                            {--orm=eloquent : The ORM the entity will use (defaults to Eloquent).}
                            {--a|all : Generate all files}';

    protected $description = 'Make a Smart Entity';

    protected $terms;

    protected function setTerms($name, $domain)
    {
        $this->terms = [
            'Entity' => [
                'name' => "{$name}Entity",
                'namespace' => 'Entities',
                'path' => "/Entities/{$name}Entity.php",
                'stub' => '/stubs/entity.stub',
            ],
            'Contract' => [
                'name' => "{$name}RepositoryContract",
                'namespace' => "Repositories\\{$domain}",
                'path' => "/Repositories/{$domain}/{$name}RepositoryContract.php",
                'stub' => '/stubs/repocontract.stub',
            ],
            'Eloquent' => [
                'Repository' => [
                    'name' => "{$name}Repository",
                    'namespace' => "Repositories\\{$domain}\\Eloquent",
                    'path' => "/Repositories/{$domain}/Eloquent/{$name}Repository.php",
                    'stub' => '/stubs/eloquent/repo.stub',
                ],
                'Model' => [
                    'name' => $name,
                    'namespace' => "Repositories\\{$domain}\\Eloquent",
                    'path' => "/Repositories/{$domain}/Eloquent/{$name}.php",
                    'stub' => '/stubs/eloquent/model.stub',
                ],
                'Provider' => [
                    'name' => "{$name}ServiceProvider",
                    'namespace' => "Providers",
                    'path' => "/Providers/{$name}ServiceProvider.php",
                    'stub' => '/stubs/eloquent/provider.stub',
                ],
            ],
            'Service' => [
                'name' => "{$domain}Service",
                'namespace' => "Services",
                'path' => "/Services/{$domain}Service.php",
                'stub' => '/stubs/service.stub',
            ],
            'Controller' => [
                'name' => "{$name}Controller",
                'namespace' => "Http\\Controllers",
                'path' => "/Http/Controllers/{$name}Controller.php",
                'stub' => '/stubs/controller.plain.stub',
            ],
            'ResourcefulController' => [
                'name' => "{$name}Controller",
                'namespace' => "Http\\Controllers",
                'path' => "/Http/Controllers/{$name}Controller.php",
                'stub' => '/stubs/controller.stub',
            ],
        ];
    }

    public function handle()
    {
        $name = ucfirst($this->getNameInput());
        $domain = $this->getDomainInput();
        $this->setTerms($name, $domain);

        if ($this->option('all')) {
            $this->input->setOption('migration', true);
            $this->input->setOption('service', true);
            $this->input->setOption('resource', true);
        }

        $this->type = 'Entity';
        parent::handle();
        $this->type = 'Contract';
        parent::handle();
        $this->type = ucfirst($this->option('orm')) . '.Repository';
        parent::handle();
        if ($this->option('orm') == 'eloquent') {
            $this->type = 'Eloquent.Model';
            parent::handle();
        }
        $this->type = ucfirst($this->option('orm')) . '.Provider';
        parent::handle();

        // Controller option
        if ($this->option('controller')) {
            $this->input->setOption('service', true);
            $this->type = 'Controller';
            parent::handle();
        }
        // Resourceful Controller option
        if ($this->option('resource')) {
            $this->input->setOption('service', true);
            $this->type = 'ResourcefulController';
            parent::handle();
        }

        // Service option
        if ($this->option('service')) {
            $this->type = 'Service';
            parent::handle();
        }

        // Migration option
        if ($this->option('migration') && $this->option('orm') == 'eloquent') {
            $this->createMigration();
        }

        // Auth command
        // Activity Log package?

        // Tell the user to add the service provider to config/app.php
        $this->alert("Make sure to add {$name}ServiceProvider to the config/app.php 'providers' array.");
    }

    protected function qualifyClass($name)
    {
        return array_get($this->terms, $this->type . '.name');
    }

    protected function getStub()
    {
        $stub = array_get($this->terms, $this->type . '.stub');
        return __DIR__ . $stub;
    }

    protected function getPath($name)
    {
        $path = array_get($this->terms, $this->type . '.path');
        return $this->laravel['path'] . $path;
    }

    protected function rootNamespace()
    {
        return parent::rootNamespace() . array_get($this->terms, $this->type . '.namespace');
    }

    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace('DummyNamespace', $this->rootNamespace(), $stub);
        return $this;
    }

    protected function replaceClass($stub, $name)
    {
        if ($this->type == 'Eloquent.Model') {
            $stub = str_replace('dummytablename', snake_case(str_plural($name)), $stub);
        }

        $stub = str_replace('DummyInput', ucfirst($this->getNameInput()), $stub);

        $stub = str_replace('DummyDomain', ucfirst($this->getDomainInput()), $stub);

        return str_replace('DummyClass', $name, $stub);
    }

    protected function createMigration()
    {
        $table = Str::plural(Str::snake(class_basename($this->argument('name'))));

        $this->call('make:migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);
    }

    protected function getDomainInput()
    {
        $domain = $this->argument('domain') ?? $this->getNameInput();
        return ucfirst($domain);
    }
}