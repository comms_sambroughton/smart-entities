<?php

namespace CommsExpress\SmartEntities\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;


class DeleteSmartEntityCommand extends Command
{
    protected $signature = 'del:smart {name} {domain?}
                            {--f|force : Force delete the domain\'s repo folder}';

    protected $description = 'Delete a Smart Entity';

    protected $terms;

    protected function setTerms($name, $domain)
    {
        $this->terms = [
            'Entity' => [
                'path' => "/Entities/{$name}Entity.php",
            ],
            'Contract' => [
                'path' => "/Repositories/{$domain}/{$name}RepositoryContract.php",
            ],
            'EloquentRepository' => [
                'path' => "/Repositories/{$domain}/Eloquent/{$name}Repository.php",
            ],
            'EloquentModel' => [
                'path' => "/Repositories/{$domain}/Eloquent/{$name}.php",
            ],
            'EloquentProvider' => [
                'path' => "/Providers/{$name}ServiceProvider.php",
            ],
            'Service' => [
                'path' => "/Services/{$name}Service.php",
            ],
            'Controller' => [
                'path' => "/Http/Controllers/{$name}Controller.php",
            ],
            'ResourcefulController' => [
                'path' => "/Http/Controllers/{$name}Controller.php",
            ],
        ];
    }

    public function handle()
    {
        $name = ucfirst($this->argument('name'));
        $domain = $this->argument('domain') ?? $name;
        $domain = ucfirst($domain);
        $this->setTerms($name, $domain);
        $rootPath = $this->laravel['path'];

        foreach ($this->terms as $key => $value) {
            $path = $rootPath . $value['path'];

            if ($key == 'Entity' && !file_exists($path)) {
                $this->info($name . 'Entity does not exist.');
                return;
            }

            if (file_exists($path)) {
                unlink($path);
                $this->info($name . $key . ' has been deleted');

                if ($key == 'EloquentProvider') $this->alert('Don\'t forget to remove ' . $name . 'ServiceProvider from config.app');
            }
        }

        if ($this->option('force')) {
            $this->rrmdir($rootPath . '/Repositories/' . $name);
        }

        $this->info("All files associated with {$name} have been deleted.");
    }

    protected function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object))
                        $this->rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            rmdir($dir);
        }
        $dir = str_replace($this->laravel['path'], '', $dir);
        $this->info($dir. ' has been deleted');
    }
}