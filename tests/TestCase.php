<?php

namespace CommsExpress\SmartEntities\Test;

use Illuminate\Database\Schema\Blueprint;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

abstract class TestCase extends OrchestraTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function createTables(array $tables)
    {
        foreach ($tables as $name => $data) {
            $this->app['db']->connection()->getSchemaBuilder()->create($name, function (Blueprint $table) use ($data) {
                $table->increments('id');
                foreach ($data as $name => $type) {
                    if (! is_array($type)) $type = [$type];
                    $col = $table->{$type[0]}($name);
                    unset($type[0]);
                    foreach ($type as $method) {
                        $col->$method();
                    }
                }
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}