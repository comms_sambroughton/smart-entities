<?php

namespace CommsExpress\SmartEntities\Test;

use CommsExpress\SmartEntities\Entities\BaseEntity;
use CommsExpress\SmartEntities\Relations\Pivot;
use CommsExpress\SmartEntities\Repositories\BaseRepositoryContract;
use CommsExpress\SmartEntities\Repositories\Eloquent\BaseModel;
use CommsExpress\SmartEntities\Repositories\Eloquent\BaseRepository;
use CommsExpress\SmartEntities\SmartEntitiesServiceProvider;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use CommsExpress\SmartEntities\Test\EloquentEntityTestEntity as Entity;
use CommsExpress\SmartEntities\Test\EloquentEntityTestRepo as Repo;
use CommsExpress\SmartEntities\Test\EloquentEntityTestModel as Model;
use CommsExpress\SmartEntities\Test\EloquentEntityTestEntityBelongsTo as EntityBelongsTo;
use CommsExpress\SmartEntities\Test\EloquentEntityTestRepoBelongsTo as RepoBelongsTo;
use CommsExpress\SmartEntities\Test\EloquentEntityTestModelBelongsTo as ModelBelongsTo;
use CommsExpress\SmartEntities\Test\EloquentEntityTestEntityBelongsToMany as EntityBelongsToMany;
use CommsExpress\SmartEntities\Test\EloquentEntityTestRepoBelongsToMany as RepoBelongsToMany;
use CommsExpress\SmartEntities\Test\EloquentEntityTestModelBelongsToMany as ModelBelongsToMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class BaseEntityTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->createTables([
            'tests' => ['name' => 'string'],
            'belongs_tos' => ['name' => 'string', 'owner_id' => ['integer', 'nullable']],
            'belongs_to_manys' => ['name' => 'string'],
            'pivot_table' => ['owner_id' => 'integer', 'owned_id' => 'integer', 'name' => ['string', 'nullable']]
        ]);
        SmartEntitiesServiceProvider::containerMap([
            Repo::class => Entity::class,
            RepoBelongsTo::class => EntityBelongsTo::class,
            RepoBelongsToMany::class => EntityBelongsToMany::class,
            Model::class => Repo::class,
            ModelBelongsTo::class => RepoBelongsTo::class,
            ModelBelongsToMany::class => RepoBelongsToMany::class,
        ]);

        $this->createTables([
            'foos' => ['name' => 'string', 'bar_id' => 'integer'],
            'bars' => ['name' => 'string', 'baz_id' => 'integer'],
            'bazs' => ['name' => 'string'],
        ]);

        SmartEntitiesServiceProvider::containerMap([
            FooRepository::class => FooEntity::class,
            Foo::class => FooRepository::class,
            BarRepository::class => BarEntity::class,
            Bar::class => BarRepository::class,
            BazRepository::class => BazEntity::class,
            Baz::class => BazRepository::class,
        ]);
    }

    /** @test */
    public function it_can_create_an_entity()
    {
        $foo = Entity::create(['name' => 'Foo']);

        $this->assertTrue($foo->exists);
        $this->assertInstanceOf(Entity::class, $foo);
        $this->assertEquals('Foo', $foo->name);
        $this->assertEquals(1, $foo->id);
    }

    /** @test */
    public function it_can_create_an_entity_using_make_and_save()
    {
        $foo = Entity::make();
        $foo->name = 'Foo';
        $foo->save();

        $this->assertCount(1, Entity::all());
        $this->assertInstanceOf(Entity::class, $foo);
        $this->assertInstanceOf(Entity::class, Entity::first());
        $this->assertEquals('Foo', Entity::first()->name);
        $this->assertEquals($foo->id, Entity::first()->id);
    }

    /** @test */
    public function it_can_update_an_entity_after_creation()
    {
        $foo = Entity::create(['name' => 'Foo']);

        $foo->name = 'Updated';
        $foo->save();
        $this->assertEquals('Updated', Entity::first()->name);

        $foo->update(['name' => 'Updated again']);
        $this->assertEquals('Updated again', Entity::first()->name);
    }

    /** @test */
    public function it_can_delete_an_entity()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $foo->delete();

        $this->assertCount(0, Entity::all());
        $this->assertFalse($foo->exists);
    }

    /** @test */
    public function it_can_soft_delete_an_entity()
    {
        Entity::$softDeletes = true;
        $foo = Entity::create(['name' => 'Foo']);

        $foo->delete();

        $this->assertCount(0, Entity::all());
        $this->assertFalse($foo->exists);
        $this->assertCount(1, Entity::withTrashed()->all());
        Entity::$softDeletes = false;
    }

    /** @test */
    public function it_can_be_restored_after_being_soft_deleted()
    {
        Entity::$softDeletes = true;
        $foo = Entity::create(['name' => 'Foo']);

        $foo->delete();
        $foo->restore();

        $this->assertCount(1, Entity::all());
        $this->assertTrue($foo->exists);
        $this->assertEquals('Foo', $foo->name);
        $this->assertEquals($foo->id, Entity::first()->id);

        Entity::$softDeletes = false;
    }

    /** @test */
    public function it_can_associate_a_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owner()->associate($bar)->save();
        $retrievedFoo = EntityBelongsTo::find($foo->id);

        $this->assertEquals('Bar', $foo->owner->name);
        $this->assertEquals('Bar', $retrievedFoo->owner->name);
        $this->assertInstanceOf(Entity::class, $foo->owner);
        $this->assertInstanceOf(Entity::class, $retrievedFoo->owner);
    }

    /** @test */
    public function it_can_dissociate_a_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $foo->owner()->associate($bar)->save();

        $foo->owner()->dissociate()->save();

        $this->assertNull($foo->owner);
        $this->assertNull(EntityBelongsTo::find($foo->id)->owner);
    }

    /** @test */
    public function it_can_attach_an_entity()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);

        $foo->owners()->attach($bar);
        $foo->owners()->attach($baz);
        $foo->save();
        $retrievedFoo = EntityBelongsToMany::with('owners')->first();

        $this->assertCount(2, $foo->owners);
        $this->assertCount(2, $retrievedFoo->owners);
        $this->assertEquals('Bar', $retrievedFoo->owners->first()->name);
        $this->assertEquals('Baz', $retrievedFoo->owners->last()->name);
        $this->assertInstanceOf(Collection::class, $foo->owners);
        $this->assertInstanceOf(Entity::class, $foo->owners->first());
        $this->assertInstanceOf(Entity::class, $retrievedFoo->owners->first());
    }

    /** @test */
    public function it_can_attach_an_entity_by_id()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owners()->attach($bar->id, ['name' => 'FooBar'])->save();

        $this->assertEquals('Bar', $foo->owners->first()->name);
        $this->assertEquals('FooBar', $foo->owners->first()->pivot->name);
        $this->assertEquals('Bar', EntityBelongsToMany::first()->owners->first()->name);
        $this->assertEquals('FooBar', EntityBelongsToMany::first()->owners->first()->pivot->name);
    }

    /** @test */
    public function it_can_attach_an_entity_by_an_array_of_ids_and_pivot_arrays()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);

        $foo->owners()->attach([
            $bar->id => ['name' => 'FooBar'],
            $baz->id => ['name' => 'FooBaz'],
        ])->save();

        $this->assertEquals('Bar', $foo->owners->first()->name);
        $this->assertEquals('FooBar', $foo->owners->first()->pivot->name);
        $this->assertEquals('Bar', EntityBelongsToMany::first()->owners->first()->name);
        $this->assertEquals('FooBar', EntityBelongsToMany::first()->owners->first()->pivot->name);
        $this->assertEquals('Baz', $foo->owners->last()->name);
        $this->assertEquals('FooBaz', $foo->owners->last()->pivot->name);
        $this->assertEquals('Baz', EntityBelongsToMany::first()->owners->last()->name);
        $this->assertEquals('FooBaz', EntityBelongsToMany::first()->owners->last()->pivot->name);
    }

    /** @test */
    public function it_can_attach_a_relation_with_pivot_data()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owners()->attach($bar, ['name' => 'FooBar'])->save();
        $retrievedFoo = EntityBelongsToMany::first();

        $this->assertInstanceOf(Pivot::class, $foo->owners->first()->pivot);
        $this->assertInstanceOf(Pivot::class, $retrievedFoo->owners->first()->pivot);
        $this->assertEquals('FooBar', $foo->owners->first()->pivot->name);
        $this->assertEquals('FooBar', $retrievedFoo->owners->first()->pivot->name);
    }

    /** @test */
    public function it_can_attach_to_a_retrieved_entity_with_pivot_data()
    {
        $template = EntityBelongsToMany::create(['name' => 'Foo']);
        $header = Entity::create(['name' => 'Bar']);

        EntityBelongsToMany::first()->owners()->attach($header, ['name' => 'Hello World'])->save();
        $this->assertEquals('Hello World', $template->owners->first()->pivot->name);
        $this->assertEquals('Hello World', EntityBelongsToMany::first()->owners->first()->pivot->name);
    }
    
    /** @test */
    public function it_can_detach_relations()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);

        $foo->owners()->attach($bar)->save();
        $foo->owners()->attach($baz)->save();
        $foo->owners()->detach($bar)->save();

        $retrievedFoo = EntityBelongsToMany::first();
        $this->assertCount(1, $foo->owners);
        $this->assertCount(1, $retrievedFoo->owners);
        $this->assertEquals('Baz', $retrievedFoo->owners->first()->name);
    }

    /** @test */
    public function it_can_create_an_entity_through_a_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = $foo->owner()->create(['name' => 'Bar']);

        $this->assertEquals('Bar', $foo->owner->name);
        $this->assertEquals('Bar', EntityBelongsTo::find($foo->id)->owner->name);
    }

    /** @test */
    public function it_can_update_an_entity_through_a_belongs_to_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owner()->associate($bar)->save();
        $foo->owner()->update(['name' => 'Updated Bar']);

        $this->assertEquals('Updated Bar', $foo->owner->name);
        $this->assertEquals('Updated Bar', Entity::first()->name);

    }

    /** @test */
    public function it_can_update_an_entity_through_a_belongs_to_many_relation()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owners()->attach($bar)->save();
        $foo->owners->first()->update(['name' => 'Updated Bar']);

        $this->assertEquals('Updated Bar', $foo->owners->first()->name);
        $this->assertEquals('Updated Bar', Entity::first()->name);

    }

    /** @test */
    public function it_can_delete_an_entity_through_a_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owner()->associate($bar)->save();
        $foo->owner()->delete();

        $this->assertNull($foo->owner);
        $this->assertCount(0, $bar->all());
    }

    /** @test */
    public function it_can_query_a_relation()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $barA = Entity::create(['name' => 'Bar']);
        $barB = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);
        $foo->owners()->attach([$barA->id => [], $barB->id => [], $baz->id => []])->save();

        $result = EntityBelongsToMany::first()->owners()->where('name', 'Bar');

        $this->assertCount(2, $result->all());
        $this->assertEquals('Bar', $result->first()->name);
        $this->assertEquals('Bar', $result->last()->name);
    }
    /** @test */
    public function querying_a_relation_will_be_limited_to_the_relations()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $barA = Entity::create(['name' => 'Bar']);
        $barB = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);
        $bazB = Entity::create(['name' => 'BazB']);
        $foo->owners()->attach([$barA->id => [], $barB->id => [], $baz->id => []])->save();

        $result = EntityBelongsToMany::first()->owners()->where('name', 'BazB');

        $this->assertCount(0, $result->all());
    }

    /** @test */
    public function it_can_retrieve_the_first_saved_entity()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $first = Entity::first();

        $this->assertInstanceOf(Entity::class, $first);
        $this->assertEquals($foo->id, $first->id);
        $this->assertEquals('Foo', $first->name);
        $this->assertTrue($first->exists);
    }

    /** @test */
    public function it_can_retrieve_all_saved_entities()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $all = Entity::all();

        $this->assertInstanceOf(Collection::class, $all);
        $this->assertInstanceOf(Entity::class, $all->first());
        $this->assertInstanceOf(Entity::class, $all->last());
        $this->assertEquals('Foo', $all->first()->name);
        $this->assertEquals('Bar', $all->last()->name);
    }

    /** @test */
    public function it_can_retrieve_an_entity_by_its_key()
    {
        $foo = Entity::create(['name' => 'Foo']);

        $find = Entity::find($foo->getKey());

        $this->assertInstanceOf(Entity::class, $find);
        $this->assertEquals('Foo', $find->name);
    }

    /** @test */
    public function it_can_retrieve_using_a_where_clause()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $where = Entity::where('name', 'Foo')->get();

        $this->assertInstanceOf(Collection::class, $where);
        $this->assertCount(1, $where);
        $this->assertInstanceOf(Entity::class, $where->first());
        $this->assertEquals('Foo', $where->first()->name);
    }

    /** @test */
    public function can_use_where_with_dot_notation()
    {
        $baz = BazEntity::create(['name' => 'Baz']);
        $bar = BarEntity::create(['name' => 'Bar', 'baz_id' => $baz->id]);
        $foo = FooEntity::create(['name' => 'Foo', 'bar_id' => $bar->id]);

        $whereHas = FooEntity::where('bar.baz.name', 'Baz')->get();
        $this->assertEquals('Foo', $whereHas->first()->name);
    }

    /** @test */
    public function it_can_eager_load_relations()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);
        $foo->owners()->attach($bar);
        $foo->owners()->attach($baz);
        $foo->save();

        $retrievedFoo = EntityBelongsToMany::with('owners')->first();

        DB::enableQueryLog();
        $this->assertCount(2, $retrievedFoo->owners);
        $this->assertCount(0, DB::getQueryLog());

        DB::disableQueryLog();
    }

    /** @test */
    public function it_can_retrieve_using_an_orwhere_clause()
    {
        Entity::create(['name' => 'Foo']);
        Entity::create(['name' => 'Bar']);
        Entity::create(['name' => 'Baz']);

        $entities = Entity::where('name','Foo')->orWhere('name','Bar')->get();

        $this->assertCount(2,$entities);
    }

    /** @test */
    public function it_can_update_pivot_data()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owners()->attach($bar, ['name' => 'A created pivot.'])->save();
        $foo->owners()->updateExistingPivot($bar->id,['name'=>'An updated pivot.']);

        $this->assertEquals('An updated pivot.', $foo->owners->first()->pivot->name);
        $this->assertEquals('An updated pivot.', EntityBelongsToMany::first()->owners->first()->pivot->name);
    }

    /** @test */
    public function it_can_sync_many_relations()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);
        $bob = Entity::create(['name' => 'Bob']);

        $foo->owners()->attach($bar)->save();
        $foo->owners()->attach($baz)->save();
        $foo->owners()->attach($bob)->save();

        $this->assertCount(3,EntityBelongsToMany::first()->owners);

        $foo->owners()->sync([$bar->id, $baz->id]);

        $this->assertCount(2,EntityBelongsToMany::first()->owners);
    }
}

class EloquentEntityTestEntity extends BaseEntity
{
    public function __construct(Repo $repo)
    {
        parent::__construct($repo);
    }

}

class EloquentEntityTestRepo extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}

class EloquentEntityTestModel extends BaseModel
{
    protected $table = 'tests';
}

class EloquentEntityTestEntityBelongsTo extends BaseEntity
{
    public function __construct(RepoBelongsTo $repo)
    {
        parent::__construct($repo);
    }

}

class EloquentEntityTestRepoBelongsTo extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(ModelBelongsTo $model)
    {
        parent::__construct($model);
    }
}

class EloquentEntityTestModelBelongsTo extends BaseModel
{
    protected $table = 'belongs_tos';

    public function owner()
    {
        return $this->belongsTo(Model::class, 'owner_id');
    }
}

class EloquentEntityTestEntityBelongsToMany extends BaseEntity
{
    public function __construct(RepoBelongsToMany $repo)
    {
        parent::__construct($repo);
    }

}

class EloquentEntityTestRepoBelongsToMany extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(ModelBelongsToMany $model)
    {
        parent::__construct($model);
    }
}

class EloquentEntityTestModelBelongsToMany extends BaseModel
{
    protected $table = 'belongs_to_manys';

    public function owners()
    {
        return $this->belongsToMany(Model::class, 'pivot_table', 'owner_id', 'owned_id')
            ->withPivot('name');
    }
}

class FooEntity extends BaseEntity
{
    public function __construct(FooRepository $repo)
    {
        parent::__construct($repo);
    }
}

class FooRepository extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(Foo $model)
    {
        parent::__construct($model);
    }
}

class Foo extends BaseModel
{
    public function bar()
    {
        return $this->belongsTo(Bar::class);
    }
}

class BarEntity extends BaseEntity
{
    public function __construct(BarRepository $repo)
    {
        parent::__construct($repo);
    }
}

class BarRepository extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(Bar $model)
    {
        parent::__construct($model);
    }
}

class Bar extends BaseModel
{
    public function baz()
    {
        return $this->belongsTo(Baz::class);
    }
}
class BazEntity extends BaseEntity
{
    public function __construct(BazRepository $repo)
    {
        parent::__construct($repo);
    }
}

class BazRepository extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(Baz $model)
    {
        parent::__construct($model);
    }
}

class Baz extends BaseModel
{

}